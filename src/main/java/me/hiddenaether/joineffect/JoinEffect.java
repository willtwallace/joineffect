package me.hiddenaether.joineffect;



import net.md_5.bungee.api.ChatColor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.yaml.snakeyaml.Yaml;



public class JoinEffect extends JavaPlugin implements Listener {

    // On server start
    @Override
    public void onEnable() {

        // Registering plugin as a JavaPlugin
        getServer().getPluginManager().registerEvents(this, this);

        // Setting up configuration file
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();

        // Print version info to console and that enable was successful
        getLogger().info("JoinEffect v4.2.5 has been successfully enabled!");

    }

    // On server stop
    @Override
    public void onDisable() {

        // Print that the plugin successfully disabled
        getLogger().info("JoinEffect has been successfully disabled!");

    }



    private Color getColor(int i) {

        Color c = null;

        if (i == 1) {
            c = Color.AQUA;
        }
        if (i == 2) {
            c = Color.BLACK;
        }
        if (i == 3) {
            c = Color.BLUE;
        }
        if (i == 4) {
            c = Color.FUCHSIA;
        }
        if (i == 5) {
            c = Color.GRAY;
        }
        if (i == 6) {
            c = Color.GREEN;
        }
        if (i == 7) {
            c = Color.LIME;
        }
        if (i == 8) {
            c = Color.MAROON;
        }
        if (i == 9) {
            c = Color.NAVY;
        }
        if (i == 10) {
            c = Color.OLIVE;
        }
        if (i == 11) {
            c = Color.ORANGE;
        }
        if (i == 12) {
            c = Color.PURPLE;
        }
        if (i == 13) {
            c = Color.RED;
        }
        if (i == 14) {
            c = Color.SILVER;
        }
        if (i == 15) {
            c = Color.TEAL;
        }
        if (i == 16) {
            c = Color.WHITE;
        }
        if (i == 17) {
            c = Color.YELLOW;
        }
        return c;

    }



    // Move this shit to its own class
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        //Defining the player for the event handler
        Player player = event.getPlayer();



        //This is the `RemoveEffects` snippet, this removes all effects on join and re-applies the enabled effects in the config
        if ((getConfig().getBoolean("RemoveEffects")) &&
                (player.hasPermission("joineffect.remove"))) {
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
        }



        if ((getConfig().getBoolean("JumpEnabled")) &&
                (player.hasPermission("joineffect.effect.jump"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, getConfig().getInt("JumpTime"), getConfig().getInt("JumpLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("SpeedEnabled")) &&
                (player.hasPermission("joineffect.effect.speed"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, getConfig().getInt("SpeedTime"), getConfig().getInt("SpeedLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("ConfusionEnabled")) &&
                (player.hasPermission("joineffect.effect.confusion"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, getConfig().getInt("ConfusionTime"), getConfig().getInt("ConfusionLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("WitherEnabled")) &&
                (player.hasPermission("joineffect.effect.wither"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, getConfig().getInt("WitherTime"), getConfig().getInt("WitherLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("BlindnessEnabled")) &&
                (player.hasPermission("joineffect.effect.blindness"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, getConfig().getInt("BlindnessTime"), getConfig().getInt("BlindnessLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("DamageResistanceEnabled")) &&
                (player.hasPermission("joineffect.effect.damage"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, getConfig().getInt("DamageResistanceTime"), getConfig().getInt("DamageResistanceLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("FastDiggingEnabled")) &&
                (player.hasPermission("joineffect.effect.fastdigging"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, getConfig().getInt("FastDiggingTime"), getConfig().getInt("FastDiggingLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("FireResitanceEnabled")) &&
                (player.hasPermission("joineffect.effect.fireresistance"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, getConfig().getInt("FireResitanceTime"), getConfig().getInt("FireResitanceLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("HarmEnabled")) &&
                (player.hasPermission("joineffect.effect.harm"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.HARM, getConfig().getInt("HarmTime"), getConfig().getInt("HarmLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("HealEnabled")) &&
                (player.hasPermission("joineffect.effect.heal"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, getConfig().getInt("HealTime"), getConfig().getInt("HealLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("HungerEnabled")) &&
                (player.hasPermission("joineffect.effect.hunger"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, getConfig().getInt("HungerTime"), getConfig().getInt("HungerLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("StrengthEnabled")) &&
                (player.hasPermission("joineffect.effect.strength"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, getConfig().getInt("StrengthTime"), getConfig().getInt("StrenghtLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("InvisibilityEnabled")) &&
                (player.hasPermission("joineffect.effect.invisible"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, getConfig().getInt("InvisibilityTime"), getConfig().getInt("InvisibilityLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("NightVisionEnabled")) &&
                (player.hasPermission("joineffect.effect.nightvision"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, getConfig().getInt("NightVisionTime"), getConfig().getInt("NightVisionLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("PoisonEnabled")) &&
                (player.hasPermission("joineffect.effect.poison"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, getConfig().getInt("PoisonTime"), getConfig().getInt("PoisonLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("RegenerationEnabled")) &&
                (player.hasPermission("joineffect.effect.regeneration"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, getConfig().getInt("RegenerationTime"), getConfig().getInt("RegenerationLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("SlownessEnabled")) &&
                (player.hasPermission("joineffect.effect.slowness"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, getConfig().getInt("SlownessTime"), getConfig().getInt("SlownessLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("SlowDiggingEnabled")) &&
                (player.hasPermission("joineffect.effect.slowdigging"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, getConfig().getInt("SlowDiggingTime"), getConfig().getInt("SlowDiggingLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("WaterBreathingEnabled")) &&
                (player.hasPermission("joineffect.effect.waterbreathing"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, getConfig().getInt("WaterBreathingTime"), getConfig().getInt("WaterBreathingLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("WeaknessEnabled")) &&
                (player.hasPermission("joineffect.effect.weakness"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, getConfig().getInt("WeaknessTime"), getConfig().getInt("WeaknessLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("SaturationEnabled")) &&
                (player.hasPermission("joineffect.effect.saturation"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, getConfig().getInt("SaturationTime"), getConfig().getInt("SaturationLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("HealthBoostEnabled")) &&
                (player.hasPermission("joineffect.effect.healthboost"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, getConfig().getInt("HealthBoostTime"), getConfig().getInt("HealthBoostLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("GlwoingEnabled")) &&
                (player.hasPermission("joineffect.effect.glowing"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, getConfig().getInt("GlowingTime"), getConfig().getInt("GlowingLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("LevitationEnabled")) &&
                (player.hasPermission("joineffect.effect.levitation"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, getConfig().getInt("LevitationTime"), getConfig().getInt("LevitationLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("LuckEnabled")) &&
                (player.hasPermission("joineffect.effect.luck"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.LUCK, getConfig().getInt("LuckTime"), getConfig().getInt("LuckLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("UnluckEnabled")) &&
                (player.hasPermission("joineffect.effect.unluck"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.UNLUCK, getConfig().getInt("UnluckTime"), getConfig().getInt("UnluckLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("DolphinsGraceEnabled")) &&
                (player.hasPermission("joineffect.effect.dolphinsgrace"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.DOLPHINS_GRACE, getConfig().getInt("DolphinsGraceTime"), getConfig().getInt("DolphinsGraceLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("ConduitPowerEnabled")) &&
                (player.hasPermission("joineffect.effect.conduitpower"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.CONDUIT_POWER, getConfig().getInt("ConduitPowerTime"), getConfig().getInt("ConduitPowerLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("DarknessEnabled")) &&
                (player.hasPermission("joineffect.effect.darkness"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.DARKNESS, getConfig().getInt("DarknessTime"), getConfig().getInt("DarknessLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("HeroOfTheVillageEnabled")) &&
                (player.hasPermission("joineffect.effect.hotv"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.HERO_OF_THE_VILLAGE, getConfig().getInt("HeroOfTheVillageTime"), getConfig().getInt("HeroOfTheVillageLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }
        if ((getConfig().getBoolean("BadOmenEnabled")) &&
                (player.hasPermission("joineffect.effect.badomen"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.BAD_OMEN, getConfig().getInt("BadOmenTime"), getConfig().getInt("BadOmenLevel"), false, getConfig().getBoolean("ParticlesEnabled")));
        }

        // Fireworks portion of the plugin
        //
        // Note: Also, get rid of those hard to read variables (in the fireworks section)!
        if (getConfig().getBoolean("FireworksEnabled")) {

            Player player1 = event.getPlayer();

            Firework fw = (Firework) player1.getWorld().spawnEntity(player1.getLocation(), EntityType.FIREWORK);
            FireworkMeta fwm = fw.getFireworkMeta();

            Random r = new Random();

            int rt = r.nextInt(4) + 1;
            FireworkEffect.Type type = FireworkEffect.Type.BALL;

            if (rt == 1) {
                type = FireworkEffect.Type.BALL;
            }
            if (rt == 2) {
                type = FireworkEffect.Type.BALL_LARGE;
            }
            if (rt == 3) {
                type = FireworkEffect.Type.BURST;
            }
            if (rt == 4) {
                type = FireworkEffect.Type.CREEPER;
            }
            if (rt == 5) {
                type = FireworkEffect.Type.STAR;
            }

            int r1i = r.nextInt(17) + 1;
            int r2i = r.nextInt(17) + 1;
            Color c1 = getColor(r1i);
            Color c2 = getColor(r2i);

            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();

            fwm.addEffect(effect);

            int rp = r.nextInt(2) + 1;
            fwm.setPower(rp);

            fw.setFireworkMeta(fwm);

        }

    }





}